<?php

namespace Database\Seeders;

use Carbon\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::factory(1)->create();
        \App\Models\Author::factory(1)->create();
        \App\Models\Developer::factory(1)->create();
        \App\Models\Manager::factory(1)->create();
    }
}
